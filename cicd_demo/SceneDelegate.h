//
//  SceneDelegate.h
//  cicd_demo
//
//  Created by ThanhTC on 12/27/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

